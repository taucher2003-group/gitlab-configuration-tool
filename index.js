import commandLineArgs from "command-line-args";
import {getConfigurationFiles} from "./config/reader.js";
import {convertToTask, readJson} from "./config/parser.js";
import fs from "fs";

const optionDefinitions = [
    { name: 'token', type: String },
    { name: 'api-url', type: String },
    { name: 'directory', alias: 'd', type: String },
    { name: 'trace-requests', type: Boolean },
    { name: 'log-file', alias: 'l', type: String }
];

const programOptions = commandLineArgs(optionDefinitions);
// noinspection JSUnresolvedVariable
const token = programOptions.token;
const apiBaseUrl = programOptions['api-url'];

global.traceRequests = programOptions['trace-requests'] ?? false;

global.logs = [];

const configFiles = getConfigurationFiles(programOptions.directory).map(file => `${programOptions.directory}/${file}`);
const configurations = await Promise.all(configFiles.map(readJson));
const tasks = await Promise.all(configurations.map(convertToTask));

try {
    await Promise.all(tasks.map(task => task(apiBaseUrl, token)));
} catch (e) {
    console.error(e);
}

if(global.logs.length > 0 && programOptions['log-file']) {
    const writer = fs.createWriteStream(programOptions['log-file'])
    global.logs.forEach(entry => writer.write(entry));
    writer.end();
}
