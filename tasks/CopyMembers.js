import {deleteRequest, getJson, postRequest, putRequest} from "../fetch/request.js";
import {groupNameToId} from "../fetch/groups.js";
import {projectNameToId} from "../fetch/projects.js";
import Role from "../constants/Role.js";
import {fetchUser} from "../fetch/users.js";

export function parse({from, to}) {
    if(Array.isArray(to)) {
        return (apiUrl, token) => to.map(t => execute(from, t)).forEach(t => t(apiUrl, token));
    }
    return (apiUrl, token) => execute(from, to)(apiUrl, token);
}

function execute(from, to) {
    const typeFrom = from.type;
    const nameFrom = from.name;
    const typeTo = to.type;
    const nameTo = to.name;
    const mapAccessLevel = level => {
        const role = Role.fromId(level);
        // noinspection JSUnresolvedVariable
        const target = to.map_access_levels?.[role] ?? Role.fromId(level);
        return Role[target];
    }

    return async (apiUrl, token) => {
        // noinspection JSUnresolvedVariable
        const fromId = (await (typeFrom === 'group' ? groupNameToId : projectNameToId)(apiUrl, token, nameFrom)).id;
        // noinspection JSUnresolvedVariable
        const toId = (await (typeTo === 'group' ? groupNameToId : projectNameToId)(apiUrl, token, nameTo)).id;

        // noinspection JSUnresolvedVariable
        const fromMembers = (await getJson(apiUrl, token, `/${typeToPathParam(typeFrom)}/${fromId}/members`))
            .map(member => { member.access_level = mapAccessLevel(member.access_level); return member; });

        const toMembers = (await getJson(apiUrl, token, `/${typeToPathParam(typeTo)}/${toId}/members`));

        const users = [
            ...await Promise.all(fromMembers.map(member => fetchUser(apiUrl, token, member.id))),
            ...await Promise.all(toMembers.map(member => fetchUser(apiUrl, token, member.id)))
        ];

        // noinspection JSUnresolvedVariable
        const finalFromMembers = fromMembers.filter(member => !users.find(u => u.id === member.id).bot);
        // noinspection JSUnresolvedVariable
        const finalToMembers = toMembers.filter(member => !users.find(u => u.id === member.id).bot);

        const membersToAdd = finalFromMembers.filter(member => !finalToMembers.map(m => m.id).includes(member.id));
        const membersToUpdate = finalFromMembers.filter(member => finalToMembers.map(m => m.id).includes(member.id));
        const membersToRemove = finalToMembers.filter(member => !finalFromMembers.map(m => m.id).includes(member.id));

        membersToAdd.forEach(member => addOrUpdateMember(member, postRequest, apiUrl, token, `/${typeToPathParam(typeTo)}/${toId}/members`));
        membersToUpdate.forEach(member => addOrUpdateMember(member, putRequest, apiUrl, token, `/${typeToPathParam(typeTo)}/${toId}/members/${member.id}`));
        membersToRemove.forEach(member => {
            const id = member.id;
            deleteRequest(apiUrl, token, `/${typeToPathParam(typeTo)}/${toId}/members/${id}`);
        })
    };
}

function addOrUpdateMember(member, fn, apiUrl, token, route) {
    const id = member.id;
    // noinspection JSUnresolvedVariable
    const accessLevel = member.access_level;
    // noinspection JSUnresolvedVariable
    const expiresAt = member.expires_at;

    fn(apiUrl, token, route, {
        user_id: id,
        access_level: accessLevel,
        expires_at: expiresAt
    });
}

function typeToPathParam(type) {
    return type === 'group' ? 'groups' : 'projects';
}