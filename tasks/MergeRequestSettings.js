import {fetchProjects, projectFileExists} from "../fetch/projects.js";
import {putRequest} from "../fetch/request.js";

export function parse({
                          name,
                          allow_merge_on_skipped_pipeline,
                          only_allow_merge_if_pipeline_succeeds,
                          only_allow_merge_if_all_discussions_are_resolved
                      }) {
    return async (apiUrl, token) => {
        const regex = RegExp(name, "i");
        // noinspection JSUnresolvedVariable
        const projects = await fetchProjects(apiUrl, token, project => regex.test(project.path_with_namespace));
        return Promise.all(projects.map(project => setMrSettings(project, {
            allow_merge_on_skipped_pipeline,
            only_allow_merge_if_pipeline_succeeds,
            only_allow_merge_if_all_discussions_are_resolved
        }, {apiUrl, token})));
    };
}

async function setMrSettings(project, data, {apiUrl, token}) {
    const ciEnabled = project.builds_access_level !== 'disabled'
        && await projectFileExists(apiUrl, token, project.id, project.default_branch, project.ci_config_path || ".gitlab-ci.yml");

    return putRequest(apiUrl, token, `/projects/${project.id}`, {
        allow_merge_on_skipped_pipeline: data.allow_merge_on_skipped_pipeline && ciEnabled,
        only_allow_merge_if_pipeline_succeeds: data.only_allow_merge_if_pipeline_succeeds && ciEnabled,
        only_allow_merge_if_all_discussions_are_resolved: data.only_allow_merge_if_all_discussions_are_resolved
    });
}