import {fetchProjects} from "../fetch/projects.js";
import {METHOD_FUNCTIONS} from "../fetch/request.js";
import {fetchGroups} from "../fetch/groups.js";

export function parse({name, type, settings, subpath = '', httpMethod = 'PUT'}) {
    if (type !== "project" && type !== "group") throw new Error("Unsupported type")

    return async (apiUrl, token) => {
        const regex = RegExp(name, "i");

        // noinspection JSUnresolvedVariable
        const predicate = type === "group" ? group => regex.test(group.full_path) : project => regex.test(project.path_with_namespace)
        const fetchFn = type === "group" ? fetchGroups : fetchProjects;
        const entities = await fetchFn(apiUrl, token, predicate);
        const updateFn = METHOD_FUNCTIONS[httpMethod.toLowerCase()];

        return Promise.all(entities.map(entity => updateFn(apiUrl, token, `/${type}s/${entity.id}/${subpath}`, settings)));
    }
}
