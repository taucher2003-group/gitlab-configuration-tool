import {fetchProjects} from "../fetch/projects.js";
import {getRequest, postRequest, putRequest} from "../fetch/request.js";
import {fetchGroups} from "../fetch/groups.js";

export function parse({name, type, author_email_regex, branch_name_regex, commit_committer_check,
                          commit_message_negative_regex, commit_message_regex, deny_delete_tag,
                          file_name_regex, max_file_size, member_check, prevent_secrets, reject_unsigned_commits}) {
    if (type !== "project" && type !== "group") throw new Error("Unsupported type")

    const settings = {
        author_email_regex, branch_name_regex, commit_committer_check,
        commit_message_negative_regex, commit_message_regex, deny_delete_tag,
        file_name_regex, max_file_size, member_check, prevent_secrets, reject_unsigned_commits
    };

    Object.entries(settings).forEach(([key, value]) => {
        if(value === undefined) {
            delete settings[key];
        }
    });

    return async (apiUrl, token) => {
        const regex = RegExp(name, "i");

        // noinspection JSUnresolvedVariable
        const predicate = type === "group" ? group => regex.test(group.full_path) : project => regex.test(project.path_with_namespace)
        const fetchFn = type === "group" ? fetchGroups : fetchProjects;
        const entities = await fetchFn(apiUrl, token, predicate);

        return Promise.all(entities.map(async entity => {
            const pushRule = await (await getRequest(apiUrl, token, `/${type}s/${entity.id}/push_rule`)).text();
            if (pushRule === 'null') {
                await postRequest(apiUrl, token, `/${type}s/${entity.id}/push_rule`, settings);
            } else {
                await putRequest(apiUrl, token, `/${type}s/${entity.id}/push_rule`, settings);
            }
        }));
    }
}
