import {fetchProjects} from "../fetch/projects.js";
import {getJson, putRequest} from "../fetch/request.js";

export function parse({name, cadence, keep_n, older_than, name_regex_keep, name_regex_delete, enabled}) {
    return async (apiUrl, token) => {
        const regex = RegExp(name, "i");
        // noinspection JSUnresolvedVariable
        const projects = await fetchProjects(apiUrl, token, project => regex.test(project.path_with_namespace));

        return Promise.all(projects.map(project => setExpirationPolicy(project, {
            cadence,
            keep_n,
            older_than,
            name_regex_keep,
            name_regex_delete,
            enabled
        }, {apiUrl, token})));
    };
}

async function setExpirationPolicy(project, data, {apiUrl, token}) {
    const currentData = await getJson(apiUrl, token, `/projects/${project.id}`);
    if(!currentData['container_registry_enabled']) {
        return Promise.resolve(); // Container Registry not enabled, Cleanup Policy not needed
    }
    if(equal(currentData['container_expiration_policy'], data)) {
        return Promise.resolve(); // Cleanup Policy already up to date
    }
    return putRequest(apiUrl, token, `/projects/${project.id}`, {container_expiration_policy_attributes: data})
}

function equal(currentData, newData) {
    // noinspection JSUnresolvedVariable
    return (
        currentData.cadence === newData.cadence
        && currentData.keep_n === newData.keep_n
        && currentData.older_than === newData.older_than
        && currentData.name_regex_keep === newData.name_regex_keep
        && currentData.name_regex === newData.name_regex_delete
        && currentData.enabled === newData.enabled
    );
}