import fs from "fs";
import path from "path";

const __dirname = path.resolve();

export function getConfigurationFiles(targetDirectory) {
    const directoryPath = path.join(__dirname, targetDirectory);
    return readDirectoryRecursive(directoryPath, `${__dirname}/${targetDirectory}`);
}

function readDirectoryRecursive(directory, currentPath) {
    const dirents = fs.readdirSync(directory, { withFileTypes: true });

    const files = dirents.filter(dirent => dirent.isFile()).map(dirent => `${dirent.name}`);
    const directories = dirents.filter(dirent => dirent.isDirectory());
    directories.forEach(dir => {
        const recursedFiles = readDirectoryRecursive(path.join(currentPath, dir.name), `${currentPath}/${dir.name}`).map(filename => `${dir.name}/${filename}`);
        files.push(...recursedFiles);
    });
    return files;
}