import fs from "fs";
import path from "path";

export function readJson(file) {
    return JSON.parse(fs.readFileSync(path.join(path.resolve(), file), 'utf-8'));
}

export function convertToTasks(configurations) {
    return configurations.map(convertToTask);
}

export async function convertToTask(configuration) {
    const {task, config} = configuration;
    const taskFile = await import(`../tasks/${task}.js`);
    return taskFile.parse(config);
}