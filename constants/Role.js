export default {
    OWNER: 50,
    MAINTAINER: 40,
    DEVELOPER: 30,
    REPORTER: 20,
    GUEST: 10,
    MINIMAL_ACCESS: 5,
    NO_ACCESS: 0,

    fromId(level) {
        switch (level) {
            case 50: return "OWNER";
            case 40: return "MAINTAINER";
            case 30: return "DEVELOPER";
            case 20: return "REPORTER";
            case 10: return "GUEST";
            case 5: return "MINIMAL_ACCESS";
            default: return "NO_ACCESS";
        }
    }
}