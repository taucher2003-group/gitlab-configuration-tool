FROM node:18.0.0-buster AS build
COPY package.json package.json
COPY yarn.lock yarn.lock
RUN yarn install --frozen-lockfile

FROM node:18.0.0-buster
ADD constants/ constants/
ADD config/ config/
ADD fetch/ fetch/
ADD tasks/ tasks/
COPY --from=build node_modules/ node_modules/
ADD package.json package.json
ADD index.js index.js
CMD ["node", "index.js"]
