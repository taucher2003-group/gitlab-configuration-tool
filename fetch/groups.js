import {getJson} from "./request.js";

export function fetchGroups(apiUrl, token, filter = () => true) {
    return getJson(apiUrl, token, '/groups?per_page=50000').then(groups => groups.filter(filter));
}

export function groupNameToId(apiUrl, token, name) {
    return getJson(apiUrl, token, `/groups/${encode(name)}`);
}

function encode(input) {
    return input.replaceAll('/', '%2F');
}