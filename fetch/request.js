import fetch from "node-fetch";

async function logResultIfTrace(response, ignoreStatusCodes = []) {
    if(global.traceRequests) {
        console.info(`> ${response.status} ${response.url}`);
    }

    if(response.status > 299 && !ignoreStatusCodes.includes(response.status)) {
        const clonedResponse = response.clone();
        const responseText = clonedResponse.text ? await clonedResponse.text() : '';
        global.logs.push([
            '='.repeat(20),
            `${clonedResponse.status} ${clonedResponse.url}`,
            responseText,
            ''
        ].join('\n'));
    }

    return response;
}

export const METHOD_FUNCTIONS = {
    get: getRequest,
    post: postRequest,
    put: putRequest,
    patch: patchRequest,
    delete: deleteRequest,
    head: headRequest
};

export function getRequest(apiUrl, token, path, ignoreStatusCodes = []) {
    const url = `${apiUrl}${path}`;
    if(global.traceRequests) {
        console.info(`< GET ${url}`);
    }
    return fetch(url, {
        method: "GET",
        headers: {
            'Private-Token': token
        }
    }).then(response => logResultIfTrace(response, ignoreStatusCodes));
}

export function getJson(apiUrl, token, path, ignoreStatusCodes = []) {
    return getRequest(apiUrl, token, path).then(response => response.json());
}

export function postRequest(apiUrl, token, path, body = {}, ignoreStatusCodes = []) {
    const url = `${apiUrl}${path}`;
    if(global.traceRequests) {
        console.info(`< POST ${url}`);
    }
    return fetch(url, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
            'Private-Token': token
        },
        body: JSON.stringify(body)
    }).then(response => logResultIfTrace(response, ignoreStatusCodes));
}

export function postJson(apiUrl, token, path, body = {}, ignoreStatusCodes = []) {
    return postRequest(apiUrl, token, path, body).then(response => response.json());
}

export function putRequest(apiUrl, token, path, body = {}, ignoreStatusCodes = []) {
    const url = `${apiUrl}${path}`;
    if(global.traceRequests) {
        console.info(`< PUT ${url}`);
    }
    return fetch(url, {
        method: "PUT",
        headers: {
            'Content-Type': 'application/json',
            'Private-Token': token
        },
        body: JSON.stringify(body)
    }).then(response => logResultIfTrace(response, ignoreStatusCodes));
}

export function putJson(apiUrl, token, path, body = {}, ignoreStatusCodes = []) {
    return putRequest(apiUrl, token, path, body).then(response => response.json());
}

export function patchRequest(apiUrl, token, path, body = {}, ignoreStatusCodes = []) {
    const url = `${apiUrl}${path}`;
    if(global.traceRequests) {
        console.info(`< PATCH ${url}`);
    }
    return fetch(url, {
        method: "PATCH",
        headers: {
            'Content-Type': 'application/json',
            'Private-Token': token
        },
        body: JSON.stringify(body)
    }).then(response => logResultIfTrace(response, ignoreStatusCodes));
}

export function patchJson(apiUrl, token, path, body = {}, ignoreStatusCodes = []) {
    return patchRequest(apiUrl, token, path, body).then(response => response.json());
}

export function deleteRequest(apiUrl, token, path, body = {}, ignoreStatusCodes = []) {
    const url = `${apiUrl}${path}`;
    if(global.traceRequests) {
        console.info(`< DELETE ${url}`);
    }
    return fetch(url, {
        method: "DELETE",
        headers: {
            'Content-Type': 'application/json',
            'Private-Token': token
        },
        body: JSON.stringify(body)
    }).then(response => logResultIfTrace(response, ignoreStatusCodes));
}

export function deleteJson(apiUrl, token, path, body = {}, ignoreStatusCodes = []) {
    return deleteRequest(apiUrl, token, path, body).then(response => response.json());
}

export function headRequest(apiUrl, token, path, ignoreStatusCodes = [404]) {
    const url = `${apiUrl}${path}`;
    if(global.traceRequests) {
        console.info(`< HEAD ${url}`);
    }
    return fetch(url, {
        method: "HEAD",
        headers: {
            'Private-Token': token
        }
    }).then(response => logResultIfTrace(response, ignoreStatusCodes));
}
