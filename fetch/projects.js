import {getJson, headRequest} from "./request.js";

export function fetchProjects(apiUrl, token, filter = () => true) {
    return getJson(apiUrl, token, '/projects?per_page=50000').then(projects => projects.filter(filter));
}

export function projectNameToId(apiUrl, token, name) {
    return getJson(apiUrl, token, `/projects/${encode(name)}`);
}

export function projectFileExists(apiUrl, token, projectName, branch, file) {
    return headRequest(apiUrl, token, `/projects/${encode(projectName)}/repository/files/${encodeURIComponent(file)}?ref=${encodeURIComponent(branch)}`)
        .then(response => response.status === 200);
}

function encode(input) {
    if(typeof input === 'number') {
        return input;
    }
    return input.replaceAll('/', '%2F');
}