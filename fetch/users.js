import {getJson} from "./request.js";

const userCache = {};

export async function fetchUser(apiUrl, token, id) {
    if(userCache[id] !== undefined) {
        return userCache[id];
    }
    const user = await getJson(apiUrl, token, `/users/${id}`);
    userCache[id] = user;
    return user;
}